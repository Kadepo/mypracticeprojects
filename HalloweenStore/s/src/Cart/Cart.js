import React from "react";
import style from "./Cart.module.css"
import {FaShoppingCart} from "react-icons/fa"
import {observer} from "mobx-react-lite";
import BasketShow from "../services/BasketShow";


export const Cart = observer((props) => {
    const {quantity = 0} = props

    return(
        <div className={style.wrapper} onClick={BasketShow.handleBasketShow}>
            <FaShoppingCart className={style.icon}/>
            {quantity ? <span className={style.cartQuantity}>{quantity}</span> : null}
        </div>
    )
})