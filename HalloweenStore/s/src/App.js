import React, {useEffect} from "react";
import {Header} from "./page/header/Header";
import {MainPage} from "./page/MainPage/MainPage";
import {Footer} from "./page/Footer/Footer";
import {ScrollUp} from "./page/ScrollUp/ScrollUp";
import ScrollReveal from "scrollreveal";
import {Cart} from "./Cart/Cart";

function App() {
    useEffect(() => {
        const sr = ScrollReveal({
            origin: 'top',
            distance: '60px',
            duration: 2500,
            delay: 400,
            // reset: true
        })
        sr.reveal('.mySwiper, .new_swiper, .newsletter_container')
        sr.reveal('.category_data, .shop_content, .content', {interval: 100})
        sr.reveal('.about_data, .discount_img', {origin: 'left'})
        sr.reveal('.about_img, .discount_data', {origin: 'right'})
    }, [])

    return (
      <>
          <Header/>
          <MainPage/>
          <Footer/>
          <ScrollUp/>
      </>

  );
}

export default App;
