import React from "react";
import style from "./FooterCard.module.css"

export const FooterCard = (props) => {
    const {title, links1, links2, links3} = props
    return(
        <div className='content'>
            <h3 className={style.title}>{title}</h3>

            <ul className={style.links}>
                <li>
                    <a href="#" className={style.links}>{links1}</a>
                </li>
                <li>
                    <a href="#" className={style.links}>{links2}</a>
                </li>
                <li>
                    <a href="#" className={style.links}>{links3}</a>
                </li>
            </ul>
        </div>

    )
}