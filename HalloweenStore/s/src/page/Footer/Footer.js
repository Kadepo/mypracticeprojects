import React from "react";
import style from "./Footer.module.css"
import logo from "../../assets/img/logo.png"
import {FaFacebookF} from "react-icons/fa"
import {AiFillInstagram} from "react-icons/ai"
import {IoLogoTwitter} from "react-icons/io"
import {FooterCard} from "./FooterCard/FooterCard";
import img1 from "../../assets/img/footer1-img.png"
import img2 from "../../assets/img/footer2-img.png"

export const Footer = () => {
    const items = [
        {id: [0], title:['About'], links1:['About Us'], links2:['Features'], links3:['News']},
        {id: [1], title:['Our Services'], links1:['Pricing'], links2:['Discounts'], links3:['Shipping mode']},
        {id: [2], title:['Our Company'], links1:['Blog'], links2:['About us'], links3:['Our mision']},
    ]
    return(
        <div className={style.footer}>
            <div className={style.container}>
                <div className='content'>
                    <a href="#" className={style.logo}>
                        <img src={logo} alt="" className={style.logoImg}/>
                            Halloween
                    </a>

                    <p className={style.description}>Enjoy the scariest night <br/> of your life.</p>

                    <div className={style.social}>
                        <a href="https://www.facebook.com/" target="_blank" className={style.socialLink}>
                            <FaFacebookF/>
                        </a>
                        <a href="https://www.instagram.com/" target="_blank" className={style.socialLink}>
                            <AiFillInstagram/>
                        </a>
                        <a href="https://twitter.com/" target="_blank" className={style.socialLink}>
                            <IoLogoTwitter/>
                        </a>
                    </div>
                </div>
                {items.map(i => <FooterCard key={i.id} {...i}/>)}
            </div>
            <span className={style.copy}>&#169; Yusupov. All rigths reserved</span>

            <img src={img1} alt="" className={style.imgOne}/>
                <img src={img2} alt="" className={style.imgTwo}/>
        </div>
    )
}