import React, {useState} from "react";
import style from "./Shop.module.css";
import {CardItem} from "./CardItem/CardItem";
import {Cart} from "../../../Cart/Cart";
import {observer} from "mobx-react-lite";
import BasketShow from "../../../services/BasketShow";
import {BasketList} from "../BasketList/BasketList";

export const Shop = observer(() => {
    const items = [
        {id: 0, title: ['Toffee'], subtitle: ['Candy'], price:['11.99'], img:['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/trick-treat1-img.png']},
        {id: 1, title: ['Bone'], subtitle: ['Accessory'], price:['8.99'], img:['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/trick-treat2-img.png']},
        {id: 2, title: ['Scarecrow'], subtitle: ['Accessory'], price:['15.99'], img:['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/trick-treat3-img.png']},
        {id: 3, title: ['Candy Cane'], subtitle: ['Candy'], price:['7.99'], img:['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/trick-treat4-img.png']},
        {id: 4, title: ['Pumpkin'], subtitle: ['Candy'], price:['19.99'], img:['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/trick-treat5-img.png']},
        {id: 5, title: ['Ghost'], subtitle: ['Accessory'], price:['17.99'], img:['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/trick-treat6-img.png']},
    ]
    const [order, setOrder] = useState([])

    const addToBasket = (item) => {
        const itemIndex = order.findIndex(orderItem => orderItem.id === item.id)

        if (itemIndex < 0) {
            const newItem = {
                ...item,
                quantity: 1
            }
            setOrder([...order, newItem])
        } else {
            const newOrder = order.map((orderItem, index) => {
                if (index === itemIndex) {
                    return {
                        ...orderItem,
                        quantity: orderItem.quantity + 1
                    }
                } else {
                    return orderItem
                }
            })
            setOrder(newOrder)
        }
    }
    console.log(order)

    const removeFromBasket = (itemId) => {
        const newOrder = order.filter(el => el.id !== itemId)
        setOrder(newOrder)
    }

    const incQuantity = (itemId) => {
        const newOrder = order.map(el => {
            if (el.id === itemId) {
                const newQuantity = el.quantity + 1
                return {
                    ...el,
                    quantity: newQuantity
                }
            } else {
                return el
            }
        })
        setOrder(newOrder)
    }

    const decQuantity = (itemId) => {
        const newOrder = order.map(el => {
            if (el.id === itemId) {
                const newQuantity = el.quantity - 1
                return {
                    ...el,
                    quantity: newQuantity >= 0 ? newQuantity : 0
                }
            } else {
                return el
            }
        })
        setOrder(newOrder)
    }

    return(
        <section className={style.wrapper} id="trick">
            <h2 className={style.title}>Trick Or Treat</h2>
            <div className={style.container}>
                {items.map(i => <CardItem key={i.id} {...i} addToBasket={addToBasket}/>)}
            </div>
            <Cart className={style.cart} quantity={order.length}/>
            {BasketShow.showState && <BasketList order={order} removeFromBasket={removeFromBasket} incQuantity={incQuantity} decQuantity={decQuantity} />}
        </section>
    )
})