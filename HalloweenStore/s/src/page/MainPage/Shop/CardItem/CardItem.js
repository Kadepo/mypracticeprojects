import React from "react";
import style from "./CardItem.module.css"
import {BiCartAlt} from "react-icons/bi"


export const CardItem =  (props) => {
    const {id, title, subtitle, price, img, addToBasket = Function.prototype}=props
    return(
        <div className={`shop_content ${style.content}`}>
            <img src={img} alt="" className={style.img}/>
            <h3 className={style.title}>{title}</h3>
            <span className={style.subtitle}>{subtitle}</span>
            <span className={style.price}>${price}</span>
            <button className={style.button} onClick={() => addToBasket({id, title, price, img})}>
                <BiCartAlt className={style.icon}/>
            </button>
        </div>
    )
}