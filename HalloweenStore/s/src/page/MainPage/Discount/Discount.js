import React from "react";
import style from "./Discount.module.css"
import img from "../../../assets/img/discount-img.png"

export const Discount = () => {
    return(
        <section className={style.discount}>
            <div className={style.container}>
                <div className={`discount_data ${style.data}`}>
                    <h2 className={style.title}>50% Discount <br/> On New Products</h2>
                    <a href="#" className={style.button}>Go to new</a>
                </div>

                <img src={img} alt="" className={`discount_img ${style.img}`}/>
            </div>
        </section>
    )
}