import React from "react";
import style from "./ArrivalsCard.module.css"
import {BiCartAlt} from "react-icons/bi"

export const ArrivalsCard = (props) => {
    const {id, img, title, subtitle, price, discount} = props
    return(
        <div className={style.content}>
            <div className={style.tag}>New</div>
            <img src={img} alt="" className={style.img}/>
                <h3 className={style.title}>{title}</h3>
                <span className={style.subtitle}>{subtitle}</span>

                <div className={style.prices}>
                    <span className={style.price}>{price}</span>
                    <span className={style.discount}>{discount}</span>
                </div>

                <button className={style.button}>
                    <BiCartAlt className={style.icon}/>
                </button>
        </div>
    )
}