import React from "react";
import style from "./Arrivals.module.css"
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import {ArrivalsCard} from "./ArrivalsCard/ArrivalsCard";

export const Arrivals = () => {
    const items = [
        {id: [1],
            img: ['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/new1-img.png'],
            title: ['Haunted House'],
            subtitle: ['Accessory'],
            price: ['$14.99'],
            discount: ['$29.99']
        },
        {id: [2],
            img: ['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/new2-img.png'],
            title: ['Halloween Candle'],
            subtitle: ['Accessory'],
            price: ['$11.99'],
            discount: ['$21.99']
        },
        {id: [3],
            img: ['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/new3-img.png'],
            title: ['Witch Hat'],
            subtitle: ['Accessory'],
            price: ['$4.99'],
            discount: ['$9.99']
        },
        {id: [4],
            img: ['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/new4-img.png'],
            title: ['Rip'],
            subtitle: ['Accessory'],
            price: ['$24.99'],
            discount: ['$44.99']
        },
        {id: [5],
            img: ['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/new5-img.png'],
            title: ['Terrifying Crystal Ball'],
            subtitle: ['Accessory'],
            price: ['$5.99'],
            discount: ['$12.99']
        },
        {id: [6],
            img: ['https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/main/assets/img/new6-img.png'],
            title: ['Witch Broom'],
            subtitle: ['Accessory'],
            price: ['$7.99'],
            discount: ['$14.99']
        },
    ]


    return(
        <section className={style.wrapper} id="new">
            <h2 className={style.title}>New Arrivals</h2>
                <div className={style.container}>
                    <Swiper slidesPerView={'auto'}
                            spaceBetween={16}
                            centeredSlides={true}
                            loop={true}
                            className='new_swiper'
                            >
                        {items.map(i => <SwiperSlide key={i.id}><ArrivalsCard {...i}/></SwiperSlide>)}
                    </Swiper>
                </div>
        </section>
    )
}