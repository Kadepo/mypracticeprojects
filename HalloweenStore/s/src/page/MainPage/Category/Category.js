import React from "react";
import style from "./Category.module.css"
import image1 from "../../../assets/img/category1-img.png"
import image2 from "../../../assets/img/category2-img.png"
import image3 from "../../../assets/img/category3-img.png"


export const Category = () => {
    return(
        <section className={style.category}>
            <h2 className={style.title}>Favorite Scare <br/> Category</h2>

            <div className={style.container}>
                <div className={`category_data ${style.data}`}>
                    <img src={image1} alt="" className={style.img}/>
                        <h3 className={style.categoryTitle}>Ghosts</h3>
                        <p className={style.description}>Choose the ghosts, the scariest there are.</p>
                </div>

                <div className={`category_data ${style.data}`}>
                    <img src={image2} alt="" className={style.img}/>
                        <h3 className={style.categoryTitle}>Pumpkins</h3>
                        <p className={style.description}>You look at the scariest pumpkins there is.</p>
                </div>

                <div className={`category_data ${style.data}`}>
                    <img src={image3} alt="" className={style.img}/>
                        <h3 className={style.categoryTitle}>Witch Hat</h3>
                        <p className={style.description}>Pick the most stylish witch hats out there.</p>
                </div>
            </div>
        </section>
    )
}