import React from "react";
import style from "./About.module.css"
import aboutImg from "../../../assets/img/about-img.png"

export const About = () => {
    return(
        <section className={style.about} id="about">
            <div className={style.container}>
                <div className={`about_data ${style.data}`}>
                    <h2 className={style.title}>About Halloween <br/> Night</h2>
                    <p className={style.description}>Night of all the saints, or all the dead, is celebrated on October
                        31 and it is a
                        very fun international celebration, this celebration comes from ancient origins, and is already
                        celebrated by everyone.
                    </p>
                    <a href="#" className={style.button}>Know more</a>
                </div>

                <img src={aboutImg} alt="" className={`about_img ${style.img}`}/>
            </div>
        </section>
    )
}
