import React from "react";
import style from "./Newsletter.module.css"

export const Newsletter = () => {
    return(
        <section className={style.wrapper}>
            <div className={`newsletter_container ${style.container}`}>
                <h2 className={style.title}>Our Newsletter</h2>
                <p className={style.description}>
                    Promotion new products and sales. Directly to your inbox
                </p>

                <form action="" className={style.newsletterForm}>
                    <input type="text" placeholder="Enter your email" className={style.newsletterInput}/>
                        <button className={style.button}>
                            Subscribe
                        </button>
                </form>
            </div>
        </section>
    )
}