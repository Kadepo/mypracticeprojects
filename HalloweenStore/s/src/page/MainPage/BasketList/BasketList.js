import React from "react";
import style from "./BasketList.module.css"
import {BasketItem} from "./BasketItem/BasketItem";
import {ImCross} from 'react-icons/im'
import {observer} from "mobx-react-lite";
import BasketShow from "../../../services/BasketShow";

export const BasketList = observer((props) => {
    const {order = [], removeFromBasket = Function.prototype, incQuantity = Function.prototype, decQuantity = Function.prototype} = props
    const totalPrice = order.reduce((sum, el) => {
        return sum + el.price * el.quantity
    }, 0)

    return(
        <div className={style.wrapper}>
            <div className={style.header}>
                <div className={style.title}>
                    <h3>Корзина</h3>
                </div>
                <div className={style.crossIcon} onClick={BasketShow.handleBasketShow}>
                    <ImCross className={style.icon}  />
                </div>
            </div>

            <div className={style.container}>
                {
                    order.length ? order.map(item => (<BasketItem key={item.id} {...item} removeFromBasket={removeFromBasket} incQuantity={incQuantity} decQuantity={decQuantity}/> )) : <h5> Добавьте товар</h5>
                }
                {order.length ? <h5>Общая стоимость: ${totalPrice}</h5> : null}
            </div>
            <div className={style.button}>
                <a>
                    Оформить
                </a>
            </div>
        </div>
    )
})