import React from "react";
import style from "./BasketItem.module.css"
import {ImCross} from 'react-icons/im'
export const BasketItem = (props) => {
    const {id, title, price, quantity, img, removeFromBasket = Function.prototype, incQuantity = Function.prototype, decQuantity = Function.prototype} = props
    return(
        <div className={style.wrapper}>
            <div className={style.container}>
                <img className={style.img} src={img} />
                {title} <button onClick={() => decQuantity(id)} >-</button>x{quantity} <button onClick={() => incQuantity(id)} >+</button> = ${price * quantity}
            </div>
            <div className={style.icon}><ImCross onClick={() => removeFromBasket(id)}/></div>
        </div>
    )
}