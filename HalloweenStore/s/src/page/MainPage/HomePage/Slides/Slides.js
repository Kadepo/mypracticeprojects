import React from "react";
import style from "./Slides.module.css"
import {BiRightArrowAlt} from "react-icons/bi"


export const Slides = (props) => {
    const { title, subtitle, img, description, dataSubtitle, dataTitle1,dataTitle2, dataTitle3}=props

    return(
        <div className={style.swiperSlide}>
            <div className={style.content}>
                <div className={style.group}>
                    <img src={img} className={style.img}/>
                        <div className={style.indicator}>

                        </div>

                        <div className={style.detailsImg}>
                            <h4 className={style.detailsTitle}>{title}</h4>
                            <span className={style.detailsSubtitle}>{subtitle}</span>
                        </div>
                </div>

                <div className={style.data}>
                    <h3 className={style.subtitle}>{dataSubtitle}</h3>
                    <h1 className={style.title}>{dataTitle1} <br/>{dataTitle2} <br/> {dataTitle3} </h1>
                    <p className={style.description}>{description}
                    </p>

                    <div className={style.buttons}>
                        <a href="#" className={style.button}>Book Now</a>
                        <a href="#" className={style.buttonLink}>
                            Track Record
                        <BiRightArrowAlt/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}