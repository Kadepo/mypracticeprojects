import React, {useEffect} from "react";
import style from "./HomePage.module.css";
import {Slides} from "./Slides/Slides";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import ScrollReveal from "scrollreveal";

export const HomePage = () => {
    const slideInfo = [
        {id: [1],
            title: ['The Labu “Reiza”'],
            subtitle: ['The Living Pumpkin'],
            img: ["https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/project-from-scratch/assets/img/home1-img.png"],
            description:
                ['Hi, I\'m Reiza, people call me "El Labu". I am currently trying to\n' + 'learn\n' + 'something new, building my own bike with parts made only in Malaysia.'],
            dataSubtitle: ['#1 Top Scariest Ghost'],
            dataTitle1: [`UOOOO`],
            dataTitle2: [`TRICK OR`],
            dataTitle3: [`TREAT!!`]},
        {id: [2],
            title: ['Adino & Grahami'],
            subtitle: ['No words can describe them'],
            img: ["https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/project-from-scratch/assets/img/home2-img.png"],
            description:
                [`Adino steals cotton candy from his brother and eats them all in one bite, a hungry beast. Grahami can no longer contain his anger towards Adino.`],
            dataSubtitle: ['#2 top Best duo'],
            dataTitle1: [`BRING BACK`],
            dataTitle2: [`MY COTTON`],
            dataTitle3: [`CANDY`]},
        {id: [3],
            title: ['Captain Sem'],
            subtitle: ['Veteran Spooky Ghost'],
            img: ["https://raw.githubusercontent.com/bedimcode/responsive-halloween-website/project-from-scratch/assets/img/home3-img.png"],
            description:
                [`In search for cute little puppy, Captain Sem has come back from his tragic death. With his hogwarts certified power he promise to be a hero for all of ghostkind.`],
            dataSubtitle: ['#3 Top Scariest  Ghost'],
            dataTitle1: [`RESPAWN`],
            dataTitle2: [`THE SPOOKY`],
            dataTitle3: [`SKULL`]}
    ]



    return(
        <section className={style.home} id="home">
            <Swiper pagination={true} modules={[Pagination]} className="mySwiper">
                {slideInfo.map(i => <SwiperSlide key={i.id}><Slides key={i.id} {...i}/></SwiperSlide>)}

            </Swiper>
        </section>
    )
}