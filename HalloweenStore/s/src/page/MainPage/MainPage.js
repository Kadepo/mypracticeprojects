import React from "react";
import style from "./MainPage.module.css"
import {HomePage} from "./HomePage/HomePage";
import {Category} from "./Category/Category";
import {About} from "./About/About";
import {Shop} from "./Shop/Shop";
import {Discount} from "./Discount/Discount";
import {Arrivals} from "./Arrivals/Arrivals";
import {Newsletter} from "./Newsletter/Newsletter";

export const MainPage = () => {
    return(
        <div className={style.main}>
            <HomePage/>
            <Category/>
            <About/>
            <Shop/>
            <Discount/>
            <Arrivals/>
            <Newsletter/>
        </div>
    )
}