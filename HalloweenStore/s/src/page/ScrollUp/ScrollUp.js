import React, {useEffect, useState} from "react";
import style from "./ScrollUp.module.css"
import {BiUpArrowAlt} from "react-icons/bi"

export const ScrollUp = () => {

    const [scroll, setScroll] = useState(0);

    const handleScroll = () => {
        setScroll(window.scrollY);
    };
    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    }, []);

    return(
        <>
            {scroll < 459 ? null : <a href="#" className={style.scroll}>
                <BiUpArrowAlt className={style.icon}/>
            </a>}
        </>

    )
}