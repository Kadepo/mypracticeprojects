import React, {useEffect,useState} from "react";
import style from "./Header.module.css"
import {BiX, BiGridAlt} from "react-icons/bi"
import logo from '../../assets/img/logo.png'
import payk from '../../assets/img/nav-img.png'
import {observer} from "mobx-react-lite";
import NavMotion from "../../services/NavMotion";

export const Header = observer(() => {

    const [indentTop, setIndentTop] = useState('-150%')

    useEffect(() => {
        if (NavMotion.navState === true) {
            setIndentTop('-150%')
        }
        if (NavMotion.navState === false) {
            setIndentTop('0')
        }
    },[NavMotion.navState])

    const slideHead = {
           top: indentTop
    }

    const [scroll, setScroll] = useState(0);

    const handleScroll = () => {
        setScroll(window.scrollY);
    };
    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    }, []);


    const color1 = {
        background: `linear-gradient(90deg, hsl(104, 28%, 40%) 0%, hsl(58, 28%, 40%) 100%)`
    }
    const color2 = {
        background: `linear-gradient(136deg, hsl(104, 28%, 35%) 0%, hsl(58, 28%, 35%) 100%)`
    }


    return(
            <header style={scroll < 49 ? color1 : color2} className={style.header}  id="header">
                <nav className={style.container}>
                    <a href="#" className={style.logo}>
                        <img src={logo} alt="" className={style.logoImg}/>
                        Halloween
                    </a>

                    <div style={slideHead} className={style.menu} id="nav-menu">
                        <ul className={style.list}>
                            <li className={style.item}>
                                <a href="#home" className={scroll < 500 ? style.activeLink : style.link} onClick={() => NavMotion.setNav()}>Home</a>
                            </li>

                            <li className={style.item}>
                                <a href="#about" className={scroll >1200 && scroll < 1750 ? style.activeLink : style.link} onClick={() => NavMotion.setNav()}>About</a>
                            </li>

                            <li className={style.item}>
                                <a href="#trick" className={scroll >1750 && scroll < 2500 ? style.activeLink : style.link} onClick={() => NavMotion.setNav()}>Candy</a>
                            </li>

                            <li className={style.item}>
                                <a href="#new" className={scroll >3100 && scroll < 3600 ? style.activeLink : style.link} onClick={() => NavMotion.setNav()}>New</a>
                            </li>

                            <a href="#" className={style.buttonGhost}>Support</a>
                        </ul>

                        <div className={style.close} id="nav-close">
                            <BiX onClick={() => NavMotion.setNav()} />
                        </div>
                        <img src={payk} alt="" className={style.img}/>
                    </div>

                    <div className={style.toggle} id="nav-toggle">
                        <BiGridAlt onClick={() => NavMotion.setNav()}/>
                    </div>

                </nav>
            </header>

        )
    }
)