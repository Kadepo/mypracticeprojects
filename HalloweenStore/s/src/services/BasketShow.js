import {makeAutoObservable} from "mobx";


class BasketShow {
    constructor() {
        makeAutoObservable(this)
    }

    showState = false

    handleBasketShow = () => {
        this.showState = !this.showState
    }

}

export default new BasketShow()