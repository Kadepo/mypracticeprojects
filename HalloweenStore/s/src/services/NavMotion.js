import {makeAutoObservable} from "mobx";


class NavMotion {
    constructor() {
        makeAutoObservable(this)
    }

    navState = true

    setNav = () => {
        this.navState = !this.navState
    }

    quanState = 0

    setQuanPlus = () => {
        this.quanState = this.quanState++
    }
    setQuanMinus = () => {
        this.quanState = this.quanState--
    }

}

export default new NavMotion()