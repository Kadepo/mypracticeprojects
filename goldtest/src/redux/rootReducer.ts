import {combineReducers} from "redux";
import {rateReducer} from "./reducers/rateReducer";

export const rootReducer = combineReducers({
    rate: rateReducer,
})

export type RootState = ReturnType<typeof rootReducer>
