import {
    FETCH_RATE,
    WEEKLY_RATE
} from "./types";
import axios from "axios";
import {IRateInfo} from "./reducers/rateReducer";



export function fetchRate() {
    return async (dispatch: any) => {
        axios.get('https://www.cbr-xml-daily.ru/daily_json.js')
            .then(res => {
                dispatch(getRate(res.data.Valute))
                dispatch(getWeeklyRate(res.data.Valute))
                const urlJson1 = res.data.PreviousURL
                axios.get(urlJson1)
                    .then(res => {
                        dispatch(getWeeklyRate(res.data.Valute))
                        const urlJson2 = res.data.PreviousURL
                        axios.get(urlJson2)
                            .then(res => {
                                dispatch(getWeeklyRate(res.data.Valute))
                                const urlJson3 = res.data.PreviousURL
                                axios.get(urlJson3)
                                    .then(res => {
                                        dispatch(getWeeklyRate(res.data.Valute))
                                        const urlJson4 = res.data.PreviousURL
                                        axios.get(urlJson4)
                                            .then(res => {
                                                dispatch(getWeeklyRate(res.data.Valute))
                                                const urlJson5 = res.data.PreviousURL
                                                axios.get(urlJson5)
                                                    .then(res => {
                                                        dispatch(getWeeklyRate(res.data.Valute))
                                                        const urlJson6 = res.data.PreviousURL
                                                        axios.get(urlJson6)
                                                            .then(res => {
                                                                dispatch(getWeeklyRate(res.data.Valute))
                                                                const urlJson7 = res.data.PreviousURL
                                                                axios.get(urlJson7)
                                                                    .then(res => {
                                                                        dispatch(getWeeklyRate(res.data.Valute))
                                                                        const urlJson8 = res.data.PreviousURL
                                                                        axios.get(urlJson8)
                                                                            .then(res => {
                                                                                dispatch(getWeeklyRate(res.data.Valute))
                                                                                const urlJson9 = res.data.PreviousURL
                                                                                axios.get(urlJson9)
                                                                                    .then(res => {
                                                                                        dispatch(getWeeklyRate(res.data.Valute))
                                                                                    })
                                                                            })
                                                                    })
                                                            })
                                                    })
                                            })
                                    })
                            })
                    })
            })
    }}


export const getRate = (rate: IRateInfo) => {
    return{
        type: FETCH_RATE,
        payload: rate
    }
}
export const getWeeklyRate = (rate: string) => {
    return{
        type: WEEKLY_RATE,
        payload: rate
    }
}

