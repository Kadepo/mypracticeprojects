import {FETCH_RATE, WEEKLY_RATE} from "../types";
import {PayloadAction} from "@reduxjs/toolkit";

export interface IRateInfo {
    ID: string,
    NumCode: string,
    CharCode: string,
    Nominal: number,
    Name: string,
    Value: any,
    Previous: any
}

export interface IRate {
    AUD: IRateInfo,
    AZN: IRateInfo,
    GBP: IRateInfo,
    AMD: IRateInfo,
    BYN: IRateInfo,
    BGN: IRateInfo,
    BRL: IRateInfo,
    HUF: IRateInfo,
    HKD: IRateInfo,
    DKK: IRateInfo,
    USD: IRateInfo,
    EUR: IRateInfo,
    INR: IRateInfo,
    KZT: IRateInfo,
    CAD: IRateInfo,
    KGS: IRateInfo,
    CNY: IRateInfo,
    MDL: IRateInfo,
    NOK: IRateInfo,
    PLN: IRateInfo,
    RON: IRateInfo,
    XDR: IRateInfo,
    SGD: IRateInfo,
    TJS: IRateInfo,
    TRY: IRateInfo,
    TMT: IRateInfo,
    UZS: IRateInfo,
    UAH: IRateInfo,
    CZK: IRateInfo,
    SEK: IRateInfo,
    CHF: IRateInfo,
    ZAR: IRateInfo,
    KRW: IRateInfo,
    JPY: IRateInfo,
}

export interface IRateState {
    fetchedRate: IRate[],
    weeklyRate: IRate[]
}

const initialState: IRateState = {
    fetchedRate: [],
    weeklyRate: []
}

export const rateReducer = (state = initialState, action: PayloadAction<any>) => {
    switch (action.type) {
        case FETCH_RATE:
            return {...state, fetchedRate: action.payload}
        case WEEKLY_RATE:
            return {...state, weeklyRate:[...state.weeklyRate, action.payload]}
        default: return state
    }
}