import React, {useEffect, useState} from "react";
import "./RateContainer.scss";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../redux/rootReducer";
import {fetchRate} from "../../redux/actions";
import {Table} from 'react-bootstrap'
import {Tooltip} from "react-tippy";



export const RateContainer = () => {

    const [weekly, setWeekly] = useState(false)
    const [coin, setCoin] = useState<any>({})


    const dispatch = useDispatch()

    useEffect(() => {
            dispatch(fetchRate())
        }, [])

    const coins = useSelector((state: RootState) => state.rate.fetchedRate)
    const weeklyCoins = useSelector((state: RootState) => state.rate.weeklyRate)
    const rateStat = weeklyCoins.map(item => item[coin.CharCode])





    return (
        <div className='wrapper'>
            <Table
                bordered
                hover >
                <thead>
                <tr>
                    <th>Код Валюта</th>
                    <th>Значение в рублях</th>
                    <th>Изменение</th>
                </tr>
                </thead>
                <tbody>
                {Object.values(coins).map((coin: any, index: number) => {
                    return (
                        <>
                        <tr key={index} onClick={() => {
                            setCoin(coin)
                            setWeekly(true)
                        }}>
                            <td>
                                <Tooltip
                                    title={coin.Name}
                                    position="bottom"
                                    trigger="mouseenter"
                                    hideDelay={0}
                                >
                                    {coin.CharCode}
                                </Tooltip>
                            </td>
                            <td>
                                {coin.Value}
                            </td>
                            <td
                                className={`${coin.Value < coin.Previous ? 'low': 'height'} `}
                            >
                                {
                                    ((coin.Value / coin.Previous) - 1).toFixed(3)
                                }
                                %
                            </td>
                        </tr>

                        </>
                    )
                })
                }
                </tbody>
            </Table>
            {weekly? <Table
                bordered
                hover>
                <thead>
                <tr>
                    <th>Код Валюта</th>
                    <th>Значение в рублях</th>
                    <th>Изменение</th>
                </tr>
                </thead>
                <tbody>
                {rateStat?.map((item: any, index: number) => {
                    return (
                        <>
                            <tr key={index}>
                                <td>
                                    <Tooltip
                                        title={item?.Name}
                                        position="bottom"
                                        trigger="mouseenter"
                                        hideDelay={0}
                                    >
                                        {item?.CharCode}
                                    </Tooltip>
                                </td>
                                <td>
                                    {item?.Value}
                                </td>
                                <td
                                    className={`${item?.Value < item?.Previous ? 'low' : 'height'} `}
                                >
                                    {
                                        ((item?.Value / item?.Previous) - 1).toFixed(3)
                                    }
                                    %
                                </td>
                            </tr>

                        </>
                    )
                })
                }
                </tbody>
            </Table>: null}
        </div>
    )
}