import React, {FC} from 'react';
import style from './App.module.css'
import {RateContainer} from "./component/RateContainer/RateContainer";



const App: FC = () =>{


  return (
    <div className={style.wrapper}>
        <RateContainer/>
    </div>
  );
}

export default App;
