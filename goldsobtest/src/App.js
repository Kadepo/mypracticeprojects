import style from './App.module.css'
import {RateContainer} from "./component/RateContainer/RateContainer";
import {useEffect, useState} from "react";
import axios from "axios";

function App() {
    const [rate, setRate] = useState([])
    useEffect(() => {
        axios('https://www.cbr-xml-daily.ru/daily_json.js')
            .then((response) => {
                console.log(response.data.Valute)
                setRate(response.data.Valute)
            })
    }, [])


  return (
      <div className={style.wrapper}>
          <div className={style.infoContainer}>
              <div>
                  <h4>Currency Name</h4>
              </div>
              <div style={{display: "flex"}}>
                  <h4>Value</h4>
                  <h4>Diff</h4>
              </div>
          </div>
          {/*{rate.map(item => (<RateContainer name={item.CharCode} value={item.Value}/>))}*/}
          <RateContainer value={rate.Value} name={rate.CharCode}/>
      </div>
  );
}

export default App;
