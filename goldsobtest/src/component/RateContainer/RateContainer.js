import React from "react";
import style from "../../App.module.css";


export const RateContainer = ({name, value}) => {
    return (
        <div className={style.infoContainer}>
            <div>
                <h3>{name}</h3>
            </div>
            <div style={{display: "flex"}}>
                <h3>{value}</h3>
                <h3>Diff</h3>
            </div>
        </div>
    )
}