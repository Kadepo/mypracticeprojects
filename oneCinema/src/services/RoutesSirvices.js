import {Routes, Route, Navigate} from 'react-router-dom'
import {MainPage} from "../pages/MainPage/MainPage";
import {TrailerPage} from "../pages/TrailerPage/TrailerPage";
import {ViewingPage} from "../pages/ViewingPage/ViewingPage";
import {ActorsPage} from "../pages/ActorsPage/ActorsPage";
import {AuthPage} from "../pages/AuthPage/AuthPage";
import {ReviewsPage} from "../pages/ReviewsPage/ReviewsPage";


export const routesSirvices = (isAuth) => {
    if (isAuth) {
        return (
            <Routes>
                <Route path={'/'} element={<MainPage/>}/>
                <Route path={'/trailer'} element={<TrailerPage/>}/>
                <Route path={'/movie'} element={<ViewingPage/>}/>
                <Route path={'/actors'} element={<ActorsPage/>}/>
                <Route path={'/Auth'} element={<MainPage/>}/>
                <Route path={'/reviews'} element={<ReviewsPage/>}/>
                <Route path={'*'} element={<Navigate to={'/'}/>}/>
            </Routes>
        )
    }

    return (
        <Routes>
            <Route path={'/'} element={<MainPage/>}/>
            <Route path={'/trailer'} element={<TrailerPage/>}/>
            <Route path={'/movie'} element={<AuthPage/>}/>
            <Route path={'/actors'} element={<AuthPage/>}/>
            <Route path={'/Auth'} element={<AuthPage/>}/>
            <Route path={'/reviews'} element={<ReviewsPage/>}/>
            <Route path={'*'} element={<Navigate to={'/'}/>}/>
        </Routes>
    )
}
