const lsName = 'reg_ozon'

export const getRegister = () => {
    return localStorage.getItem(lsName)
}

export const setRegister = (email, password) => {
    const oldLS = getRegister()
    if (oldLS === null) {
        return  localStorage.setItem(lsName, JSON.stringify([{email, password}]))
    }
    let array = JSON.parse(oldLS)
    array.push({email, password})

    localStorage.setItem(lsName, JSON.stringify(array))
}