import {makeAutoObservable} from "mobx";
import {delAuth} from "./LSAuth";

class AuthState {
    constructor() {
        makeAutoObservable(this)
    }

    isAuth = false


    setAuth = () => {
        this.isAuth = true
    }

    noAuth = () => {
        delAuth()
        this.isAuth = false
    }

}


export default new AuthState()