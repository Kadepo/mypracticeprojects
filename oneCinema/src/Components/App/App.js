import React, {useState, useEffect} from "react";
import {Header} from "../Header/Header";
import {routesSirvices} from "../../services/RoutesSirvices";
import AuthState from "../../services/AuthState";
import {observer} from "mobx-react-lite";
import {getAuth} from "../../services/LSAuth";

export const App = observer(() => {
    const isAuth = AuthState.isAuth
    const router = routesSirvices(isAuth)

    useEffect(() => {
        const checkAuth = getAuth()
        if (checkAuth) {
            AuthState.setAuth()
        }
    },[])

    return (
        <div>
            <Header />
            {router}
        </div>
    )
})