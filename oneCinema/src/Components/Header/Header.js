import React, {useEffect, useState} from "react";
import styles from "./Header.module.css"
import {NavLink} from "react-router-dom";
import {observer} from "mobx-react-lite";
import AuthState from "../../services/AuthState";

export const Header = observer(() => {
    const headMenu = [
        {id:1, name: 'ГЛАВНАЯ', link: '/'},
        {id:2, name: 'ТРЕЙЛЕР', link: '/trailer'},
        {id:3, name: 'ПРОСМОТР', link: '/movie'},
        {id:4, name: 'АКТЕРЫ', link: '/actors'},
        {id:5, name: 'ОТЗЫВЫ', link: '/reviews'}
    ]

    const headMenuItems = headMenu.map(i => <NavLink key={i.id} to={i.link}>{i.name}</NavLink>)

    const [displ1, setDispl1] = useState('grid')

    useEffect(() => {
        if(AuthState.isAuth === true) {
            setDispl1('none')
        }
        if(AuthState.isAuth === false) {
            setDispl1('grid')
        }
    },[AuthState.isAuth])

    const visionNav = {
        display: displ1
    }

    const [displ2, setDispl2] = useState('none')

    useEffect(() => {
        if(AuthState.isAuth === false) {
            setDispl2('none')
        }
        if(AuthState.isAuth === true) {
            setDispl2('grid')
        }
    },[AuthState.isAuth])

    const visionBtn = {
        display: displ2
    }


    return (
        <div className={styles.wrapper}>
            <div className={styles.head}>
                {headMenuItems}
            </div>
            <div className={styles.login}>
                <NavLink style={visionNav} to={'/Auth'} >
                    <span>Войти</span>
                </NavLink>
                <button style={visionBtn} onClick={() => AuthState.noAuth()} >
                    <span>Выйти</span>
                </button>
            </div>
        </div>
    )
})