import React from "react";
import styles from "./Comments.module.css"

export const Comments = ({id, email, post, }) => {
    return (
        <div className={styles.wrapper}>
            <h3>{email}:</h3>
            <p>{post}</p>
        </div>
    )
}