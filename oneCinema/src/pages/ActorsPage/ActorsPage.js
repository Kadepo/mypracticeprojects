import React, {useState} from "react";
import styles from "./ActorsPage.module.css"
import {ArrowUpIcon} from "../../Icons/ArrowUpIcon";
import {ArrowDownIcon} from "../../Icons/ArrowDownIcon";


export const ActorsPage = () => {

    const [activeSlideIndex, setSlideIndex] = useState(0)

    function increment() {
        setSlideIndex(activeSlideIndex + 1)
        if (activeSlideIndex === 7) {
            setSlideIndex(activeSlideIndex - 7)
        }
    }
    function decrement() {
        setSlideIndex(activeSlideIndex -1)
        if (activeSlideIndex < 1) {
            setSlideIndex(activeSlideIndex + 7)
        }
    }

    const transformSlide1 = {
        transform: `translateY(${activeSlideIndex*100}%)`,
    }
    const transformSlide2 = {
        transform: `translateY(-${activeSlideIndex*100}%)`,
    }

    return (
        <div className={styles.body}>
        <div className={styles.container}>
            <div style={transformSlide1}  className={styles.sidebar}>
                <div className={styles.sidebar8}>
                    <h1>Стэн Ли</h1>
                    <p>Ксандарианский бабник</p>
                </div>
                <div className={styles.sidebar7}>
                    <h1>Майкл Рукер</h1>
                    <p>Йонду</p>
                </div>
                <div className={styles.sidebar6}>
                    <h1>Вин Дизель</h1>
                    <p>Грут</p>
                </div>
                <div className={styles.sidebar5}>
                    <h1>Брэдли Купер</h1>
                    <p>Реактивный Енот</p>
                </div>
                <div className={styles.sidebar1}>
                    <h1>Карен Гиллан</h1>
                    <p>Небула</p>
                </div>
                <div className={styles.sidebar2}>
                    <h1>Зои Салдана</h1>
                    <p>Гамора</p>
                </div>
                <div className={styles.sidebar3}>
                    <h1>Дейв Батиста</h1>
                    <p>Дракс</p>
                </div>
                <div className={styles.sidebar4}>
                    <h1>Крис Пратт</h1>
                    <p>Звёздный Лорд</p>
                </div>
            </div>
            <div style={transformSlide2} className={styles.mainSlide}>
                <div className={styles.slide1}>

                </div>
                <div className={styles.slide2}>

                </div>
                <div className={styles.slide3}>

                </div>
                <div className={styles.slide4}>

                </div>
                <div className={styles.slide5}>

                </div>
                <div className={styles.slide6}>

                </div>
                <div className={styles.slide7}>

                </div>
                <div className={styles.slide8}>

                </div>
            </div>
            <div className={styles.controls}>
                <button onClick={decrement} className={styles.downButton}>
                    <i><ArrowDownIcon/></i>
                </button>
                <button onClick={increment} className={styles.upButton}>
                    <i><ArrowUpIcon/></i>
                </button>
            </div>
        </div>
        </div>
    )
}