import React from "react";
import styles from "./MainPage.module.css"


export const MainPage = () => {
    return (
        <div className={styles.back}>
            <div className={styles.story}>
                <p>
                    Отважному путешественнику Питеру Квиллу попадает в руки таинственный артефакт, принадлежащий могущественному и безжалостному злодею Ронану, строящему коварные планы по захвату Вселенной. Питер оказывается в центре межгалактической охоты, где жертва — он сам.
                </p>
            </div>
        </div>
    )
}