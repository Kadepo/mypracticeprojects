import React, {useState, useEffect} from "react";
import axios from "axios";
import styles from "./ReviewsPage.module.css"
import {Comments} from "../../Components/Comments/Comments";



export const ReviewsPage = () => {
    const [reviews, setReviews] = useState([])

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then(res => {
                setReviews(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    },[])

    const Commentaries = reviews.map(i => <Comments id={i.id} email={i.email} post={i.body} />)



    return (
        <div className={styles.wrapper}>
                <p className={styles.head}>Отзывы</p>
            <div>
            {reviews.length !== 0 ? Commentaries : 'Отзывов нет'}
            </div>
        </div>
    )
}