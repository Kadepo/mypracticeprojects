import React, {useState} from "react";
import styles from "./AuthPage.module.css"
import {observer} from "mobx-react-lite";
import AuthState from "../../services/AuthState";
import {setRegister, getRegister} from "../../services/LSRegister";
import {setAuth} from "../../services/LSAuth";


export const AuthPage = observer(() => {

    const [form, setForm] = useState({email: '', password: ''})

    const changeForm = (event) => {
        setForm({...form, [event.currentTarget.name]: event.currentTarget.value})
    }

    const registerHandler = () => {
        setRegister(form.email, form.password)
    }

    const loginHandler = () => {
        const users = JSON.parse(getRegister())
        if (users) {
            const candidate = users.filter(i => i.email === form.email)
            if (candidate.length !== 0) {
                if (candidate[0].password === form.password) {
                    setAuth()
                    return AuthState.setAuth()
                }
                return alert('Пароль неверный')
            }
        }
        return alert('Такого пользователя не существует')
    }

    return (
        <div className={styles.back}>
            <div className={styles.container}>
                <h1>Авторизация</h1>
                <div className={styles.input1}>
                    <label>Email:</label>
                    <input type="text" placeholder={'email'} value={form.email} onChange={changeForm} />
                    <label>Password:</label>
                    <input placeholder={'password'} value={form.password} onChange={changeForm}/>
                </div>
                <div className={styles.btn}>
                    <button onClick={loginHandler}>Вход</button>
                    <button onClick={registerHandler}>Регистрация</button>
                </div>
            </div>
        </div>
    )
})