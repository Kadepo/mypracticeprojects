import React from "react";
import styles from "./Viewing.module.css"


export const ViewingPage = () => {
    return (
        <div className={styles.back}>
            <div className={styles.movie}>
            <iframe src="https://toq1.films-s.life/iMEg0A8rnB5n?kp_id=689066" width="1450" height="765" frameBorder="0"
                    allowFullScreen=""></iframe>
            </div>
        </div>
    )
}