import React from "react";
import styles from "./Trailer.module.css"


export const TrailerPage = () => {
    return (
        <div className={styles.back}>
            <div className={styles.play}>
            <iframe width="1050" height="617" src="https://www.youtube.com/embed/nlysXqG-gbQ"
                    title="YouTube video player" frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen></iframe>
            </div>
        </div>
    )
}