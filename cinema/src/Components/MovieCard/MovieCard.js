import React from "react";
import styles from "./MovieCard.module.css"
import {category} from "../../ApiServices/tmdbApi";
import apiConfig from "../../ApiServices/ApiConfig";
import {NavLink} from "react-router-dom";
import Button from "../Button/Button";

export const MovieCard = props => {
    const item = props.item

    const link = '/' + category[props.category] + '/' + item.id

    const bg = apiConfig.w500Image(item.poster_path || item.backdrop_path)

    return (
        <NavLink to={link}>
            <div className={styles.movieCard} style={{backgroundImage: `url(${bg})`}}>
                <Button>
                    <i className={styles.bx}></i>
                </Button>
            </div>
            <h3>{item.title || item.name}</h3>
        </NavLink>
        )
}

