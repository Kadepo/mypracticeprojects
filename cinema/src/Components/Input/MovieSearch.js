import { useNavigate } from "react-router-dom";
import {category} from "../../ApiServices/tmdbApi";
import Input from "./Input";
import styles from "../pages/MoviesPage/MoviesPage.module.css";
import {useCallback, useEffect, useState} from "react";

const MovieSearch = props => {

    const navigate = useNavigate()

    const [keyword, setKeyword] = useState(props.keyword ? props.keyword : '')

    const goToSearch = useCallback(
        () => {
            if (keyword.trim().length > 0) {
                navigate(`/${category[props.category]}/search/${keyword}`)
            }
        },
        [keyword, props.category, navigate]
    )

    useEffect(() => {
        const enterEvent = (e) =>{
            e.preventDefault()
            if (e.keyCode === 13) {
                goToSearch()
            }
        }
        document.addEventListener('keyup', enterEvent)
        return () => {
            document.removeEventListener('keyup', enterEvent)
        }
    },[keyword, goToSearch])

    return(
        <div>
            <Input
                type="text"
                placeholder="Enter keyword"
                value={keyword}
                onChange={(e) => setKeyword(e.target.value)}
            />
            <button className={styles.btn} onClick={goToSearch}>Поиск</button>
        </div>
    )
}

export default MovieSearch