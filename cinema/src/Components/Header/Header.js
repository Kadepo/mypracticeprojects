import React from "react";
import styles from "./Header.module.css"
import { BiSearch } from "react-icons/bi";
import {BiBell} from "react-icons/bi"
import {NavLink} from "react-router-dom";

export const Header = () => {
    const headMenu = [
        {id:1, name:'Мой ivi', link:'/'},
        {id:2, name:'Что нового', link:'/'},
        {id:3, name:'Фильмы', link:'/movie'},
        {id:4, name:'Сериалы', link:'/tv'},
        {id:5, name:'Мультфильмы', link:'/'},
        {id:6, name:'TV+', link:'/'},
    ]

    const headMenuItems = headMenu.map(i => <NavLink key={i.id} to={i.link}>{i.name}</NavLink>)


    return (
        <div className={styles.wrapper}>
            <div className={styles.headLogo}>
                <img src="https://icons.tivision.ru/picture/ea003d,ffffff/iviLogoPlateRounded.svg" alt=""/>
            </div>
            <div className={styles.head}>
                {headMenuItems}
            </div>
            <div className={styles.rightHead}>
              <div className={styles.Sub}>
                <button className={styles.btnSub}>
                    Подписка за 99 ₽
                </button>
                <BiSearch className={styles.searchIcon}/>
                <button className={styles.search}>
                    Поиск
                </button>
              </div>
              <div className={styles.bell}>
                <BiBell/>
              </div>
              <div className={styles.avatar}>
                <button>
                    П
                </button>
              </div>
            </div>
        </div>
    )
}