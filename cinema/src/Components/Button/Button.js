import React from "react";
import styles from "./button.module.css"

const Button = props => {
    return(
        <button className= {styles.btn} onClick={props.onClick ? () => props.onCLick() : null} >
            {props.children}
        </button>
    )
}

export const OutLineButton = props => {
    return(
        <Button className= {styles.btnOutline} onClick={props.onClick ? () => props.onCLick() : null}>
            {props.children}
        </Button>
    )
}

export default Button