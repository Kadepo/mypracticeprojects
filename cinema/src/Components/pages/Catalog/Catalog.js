import React from "react";
import {useParams} from "react-router-dom";
import PageHeader from "../PageHeader/PageHeader";
import {category as cate} from "../../../ApiServices/tmdbApi";
import {MoviesPage} from "../MoviesPage/MoviesPage";

export const Catalog = () => {

    const {category} = useParams()

    console.log(category)
    return(
        <>
            <PageHeader>
                {category === cate.movie ? 'Фильмы' : 'Сериалы'}
            </PageHeader>
            <div>
                <div>
                    <MoviesPage category={category}/>
                </div>
            </div>
        </>
    )
}