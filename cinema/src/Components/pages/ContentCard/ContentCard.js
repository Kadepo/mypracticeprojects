import React, {useEffect, useState} from "react";
import styles from "./ContentCard.module.css"
import tmdbApi from "../../../ApiServices/tmdbApi";
import apiConfig from "../../../ApiServices/ApiConfig";
import {useParams} from "react-router-dom";
import CastList from "./CastList/CastList";
import VideoList from "./VideoList/VideoList";
import {MovieList} from "../MainPage/MovieList/MovieList";

export const ContentCard = () => {

    const {category, id} = useParams()

    const [item, setItem] = useState(null)

    useEffect(() => {
        const getDetail = async () => {

            const response = await tmdbApi.detail(category, id, {params:{language: 'ru-Ru'}})
            setItem(response)
            window.scrollTo(0,0)
        }
        getDetail()
    },[category, id])

    return (
        <>
            {
                item && (
                    <>
                        <div className={styles.banner} style={{backgroundImage: `url(${apiConfig.originalImage(item.backdrop_path || item.poster_path)})`}}></div>
                        <div className={styles.movieContent}>
                            <div className={styles.movieContentPoster}>
                                <div className={styles.movieContentPosterImg} style={{backgroundImage: `url(${apiConfig.originalImage(item.poster_path || item.backdrop_path)})`}}></div>
                            </div>
                            <div className={styles.movieContentInfo}>
                                <h1 className={styles.title}>
                                    {item.title || item.name}
                                </h1>
                                <div className={styles.genres}>
                                    {
                                        item.genres && item.genres.slice(0, 5).map((genre, i) => (
                                            <span key={i} className={styles.genresItem}>{genre.name}</span>
                                        ))
                                    }
                                </div>
                                <p className={styles.overview}>{item.overview}</p>
                                <div className={styles.casts}>
                                    <div className={styles.sectionHeader}>
                                        <h2>Casts</h2>
                                    </div>
                                    <CastList id={item.id}/>
                                </div>
                            </div>
                        </div>
                        <div >
                            <div >
                                <VideoList id={item.id}/>
                            </div>
                            <div>
                                <div>
                                    <h2>Similar</h2>
                                </div>
                                <MovieList category={category} type="similar" id={item.id}/>
                            </div>
                        </div>
                    </>
                )
            }
        </>
    )
}