import React, { useState, useEffect } from 'react';

import { useParams } from 'react-router';

import styles from "./CastList.module.css"

import tmdbApi from "../../../../ApiServices/tmdbApi";
import apiConfig from "../../../../ApiServices/ApiConfig";

const CastList = props => {

    const {category} = useParams();

    const [casts, setCasts] = useState([]);

    useEffect(() => {
        const getCredits = async () => {
            const res = await tmdbApi.credits(category, props.id);
            setCasts(res.cast.slice(0, 5));
        }
        getCredits();
    }, [category, props.id]);
    return (
        <div className={styles.casts}>
            {
                casts.map((item, i) => (
                    <div key={i} className={styles.castsItem}>
                        <div className={styles.castsItemImg} style={{backgroundImage: `url(${apiConfig.w500Image(item.profile_path)})`}}></div>
                        <p className={styles.castsItemName}>{item.name}</p>
                    </div>
                ))
            }
        </div>
    );
}

export default CastList;