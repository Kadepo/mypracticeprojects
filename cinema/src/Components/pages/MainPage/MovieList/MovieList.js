import React, {useEffect, useState} from "react";
import tmdbApi, {category} from "../../../../ApiServices/tmdbApi";
import {Swiper, SwiperSlide} from "swiper/react"
import {MovieCard} from "../../../MovieCard/MovieCard";
import apiConfig from "../../../../ApiServices/ApiConfig";
import style from "./MovieList.module.css"
import "swiper/css";

export const MovieList = props => {
    const [movieItems, setMovieItems] = useState([])

    useEffect(() => {
        const itemList = async () =>{
            let response = null
            const params = {language: 'ru-Ru'}

            if(props.type !== 'similar') {
                switch (props.category) {
                    case category.movie:
                        response = await tmdbApi.getMovieList(props.type, {params})
                        break
                    default:
                        response = await tmdbApi.getTvList(props.type, {params})
                }
            } else {
                response = await tmdbApi.similar(props.category, props.id)
            }
            setMovieItems(response.results)
        }
        itemList()
    })

    return(
        <div className={style.movieList}>
            <Swiper
                 spaceBetween={30} grabCursor={true}
                slidesPerView={'auto'}
            >
                {
                    movieItems.map((item, i) => (
                        <SwiperSlide key={i} className={style.slide}>
                            {/*<img src={apiConfig.w500Image(item.poster_path)}/>*/}
                            <MovieCard item={item} category={props.category}/>
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </div>
    )}