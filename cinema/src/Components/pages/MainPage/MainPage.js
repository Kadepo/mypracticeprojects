import React from "react";
import styles from "./MainPage.module.css"
import HeroSlide from "./HeroSlide/HeroSlide";
import {MovieList} from "./MovieList/MovieList";
import {category, movieType, tvType} from "../../../ApiServices/tmdbApi";

export const MainPage = () => {
    return (
        <div className={styles.wrapper}>
            <HeroSlide/>
            <h5>Фильмы</h5>
            <MovieList category={category.movie} type={movieType.popular}/>
            <h5>Топ рейтинг</h5>
            <MovieList category={category.movie} type={movieType.top_rated}/>
            <h5>Тренды Tv</h5>
            <MovieList category={category.tv} type={tvType.popular}/>
            <h5>Топ Tv</h5>
            <MovieList category={category.tv} type={tvType.top_rated}/>
        </div>
    )
}