import React, {useEffect, useState} from "react";
import tmdbApi, {movieType} from "../../../../ApiServices/tmdbApi";
import apiConfig from "../../../../ApiServices/ApiConfig";
import "swiper/css";
import styles from "./HeroSlide.module.css"
import SwiperCore, {Autoplay} from "swiper";
import {Swiper, SwiperSlide} from "swiper/react"

const HeroSlide = () => {

    SwiperCore.use([Autoplay])

     const [movieItems, setMovieItems] = useState([])

     useEffect(() => {
         const getMovies = async () => {
             const params = {page: 1, language: 'ru-Ru'}
             try {
                 const response = await tmdbApi.getMovieList(movieType.popular, {params})
                 setMovieItems(response.results.slice(0, 4))
                 console.log(response)
             } catch {
                 console.log('error')
             }
         }
         getMovies()
     }, [])

    return (
        <div className={styles.heroSlide}>
            <Swiper
                module={[Autoplay]}
                centeredSlides={true}
                autoplay={{
                    "delay": 3500,
                    "disableOnInteraction": false
                }}
                grabCursor={true}
                spaceBetween={0}
                slidesPerView={1}
            >
                {
                    movieItems.map((item, i) => (
                        <SwiperSlide>
                            {({isActive}) => (
                                <img src={apiConfig.originalImage(item.backdrop_path)} alt=""/>
                            )}
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </div>
    )
}

export default HeroSlide