import React from "react";
import {useHistory} from "react-router-dom";
import apiConfig from "../../../../ApiServices/ApiConfig";

const HeroSlideItem = props => {

    const item = props.item

    const background = apiConfig.originalImage(item.backdrop_path ? item.backdrop_path : item.poster_path)

    return(
        <div
        style={{backgroundImage: `url(${background})`}}>
            {item.title}
        </div>
    )
}

export default HeroSlideItem