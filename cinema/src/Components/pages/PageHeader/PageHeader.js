import React from "react";
import styles from "./PageHeader.module.css"

const PageHeader = props => {
    return(
        <div className={styles.pageHeader}>
            <h2>{props.children}</h2>
        </div>
    )
}

export default PageHeader