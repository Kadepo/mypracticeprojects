import React, { useEffect, useState} from "react";
import styles from "./MoviesPage.module.css";
import {MovieCard} from "../../MovieCard/MovieCard";
import MovieSearch from "../../Input/MovieSearch";
import { useParams} from "react-router-dom";
import tmdbApi, {category, movieType, tvType} from "../../../ApiServices/tmdbApi";

export const MoviesPage = props => {
    const [items, setItems] = useState([])

    const [page, setPage] = useState(1)
    const [totalPage, setTotalPage] = useState(0)

    const {keyword} = useParams()

    useEffect(() => {
        const getList = async () => {
            let response = null
            if (keyword === undefined) {
                const params = {language: 'ru-Ru'}
                switch (props.category) {
                    case category.movie:
                        response = await tmdbApi.getMovieList(movieType.upcoming, {params})
                        break
                    default:
                        response = await tmdbApi.getTvList(tvType.popular, {params})
                }
            } else {
                const params = {
                    query: keyword
                }
                response = await tmdbApi.search(props.category, {params})
            }
            setItems(response.results)
            setTotalPage(response.total_pages)
        }
        getList()
    }, [props.category, keyword])

    const loadMore = async () => {
        let response = null
        if (keyword === undefined) {
            const params = {page: page + 1, language: 'ru-Ru'}
            switch (props.category) {
                case category.movie:
                    response = await tmdbApi.getMovieList(movieType.upcoming, {params})
                    break
                default:
                    response = await tmdbApi.getTvList(tvType.popular, {params})
            }
        } else {
            const params = {
                page: page + 1,
                query: keyword
            }
            response = await tmdbApi.search(props.category, {params})
        }
        setItems([...items, ...response.results])
        setPage(page + 1)

    }

    return(
        <>
            <div className={styles.movSearch}>
                <MovieSearch category={props.category} keyword={keyword}/>
            </div>
           <div className={styles.wrapper}>
               {
                   items.map((item, i) => <MovieCard category={props.category} item={item} key={i}/>)
               }
           </div>
            {
                page < totalPage ? (
                    <div className={styles.loadmore}>
                        <button className={styles.btn} onClick={loadMore}>Показать еще</button>
                    </div>
                ) : null
            }
        </>
    )
}

