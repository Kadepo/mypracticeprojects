import {Routes, Route} from 'react-router-dom'
import {MainPage} from "../Components/pages/MainPage/MainPage";
import {ContentCard} from "../Components/pages/ContentCard/ContentCard";
import {Catalog} from "../Components/pages/Catalog/Catalog";

export const RoutesServices = () => {
    return (
        <Routes>
            <Route path={'/:category/search/:keyword'} element={<Catalog/>}/>
            <Route path={'/:category/:id'} element={<ContentCard/>}/>
            <Route path={'/:category'} element={<Catalog/>}/>
            <Route path={'/'} element={<MainPage/>}/>
        </Routes>
    )
}