const apiConfig = {
    baseURL: 'https://api.themoviedb.org/3/',
    apiKey: 'c557720bac8baefe5f3e99705aa84181',
    originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`
}

export default apiConfig