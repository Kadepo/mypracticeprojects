import React from "react";
import style from "./App.module.css"
import {Header} from "../Components/Header/Header";
import {RoutesServices} from "../Services/RoutesServices";

export const App = () => {
    const router = RoutesServices()
    return(
        <div className={style.wrapper}>
            <Header />
            {router}
        </div>
    )
}