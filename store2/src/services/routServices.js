import {Routes, Route, Navigate} from "react-router-dom"
import {MainPage} from "../Components/MainPage/MainPage";
import {Customers} from "../Components/Customers/Customers";
import {MessagePage} from "../Components/MessagePage/MessagePage";
import {HelpPage} from "../Components/HelpPage/HelpPage";
import {SettingsPage} from "../Components/SettingsPage/SettingsPage";
import {AuthPage} from "../Components/AuthPage/AuthPage";

export const routServices = () => {
    return (
        <Routes>
            <Route path={'/main'} element={<MainPage/>}/>
            <Route path={'/customers'} element={<Customers/>}/>
            <Route path={'/message'} element={<MessagePage/>}/>
            <Route path={'/help'} element={<HelpPage/>}/>
            <Route path={'/settings'} element={<SettingsPage/>}/>
            <Route path={'/auth'} element={<AuthPage/>}/>
            <Route path={'*'} element={<Navigate to={'/main'}/>}/>
        </Routes>
    )

}