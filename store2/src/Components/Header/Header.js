import React, {useEffect, useState} from "react";
import styles from "./Header.module.css"
import {MenuIcon} from "../Icons/MenuIcon";
import {SearchIcon} from "../Icons/SearchIcon";
import {observer} from "mobx-react-lite";
import NavMotion from "../../services/NavMotion";


export const Header = observer(() => {
    const [fullWidth, useWidth] = useState('calc(100% - 300px)')

    useEffect(()=> {
        if (NavMotion.navState === true) {
            useWidth('calc(100% - 300px)')
        }
        if (NavMotion.navState === false) {
            useWidth('calc(100% - 80px)')
        }
        console.log(NavMotion.navState)
    },[NavMotion.navState])

    const [indentLeft, useLeft] = useState('300px')

    useEffect(()=> {
        if (NavMotion.navState === true) {
            useLeft('300px')
        }
        if (NavMotion.navState === false) {
            useLeft('80px')
        }
        console.log(NavMotion.navState)
    },[NavMotion.navState])

    const slideHead = {
        left : indentLeft,
        width : fullWidth
    }

    return (
        <div style={slideHead} className={styles.topbar}>
            <div className={styles.toggle} onClick={() => NavMotion.setNav()}>
                <MenuIcon/>
            </div>
            <div className={styles.search}>
                <label htmlFor="">
                    <SearchIcon/>
                    <input type="text" placeholder="Search Here"/>
                </label>
            </div>
            <div className={styles.user}>
                <img src={"https://sun9-78.userapi.com/impg/8soiupLld4S6d0I4a_QuQDbJz6mUnnYQwntJdA/OTj6mm8-U_E.jpg?size=100x100&quality=96&sign=d85545466b8025c77e05f0fab3c3680f&type=album"}/>
            </div>
        </div>
    )
})