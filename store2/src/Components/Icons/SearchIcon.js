import React from "react";
import {IoSearchOutline} from "react-icons/io5";

export const SearchIcon = () => {
    return (
        <IoSearchOutline size="1.2em" />
    )
}