import React from "react";
import { IoLogoApple } from "react-icons/io5";

export const OneIcon = () => {
    return (
        <IoLogoApple size="1.75em"/>
    )
}