import React from "react";
import {IoCashOutline} from "react-icons/io5";

export const CashIcon = () => {
    return (
        <IoCashOutline />
    )
}