import React from "react";
import {IoSettingsOutline} from "react-icons/io5";

export const SettingsIcon = () => {
    return (
        <IoSettingsOutline size="1.75em"/>
    )
}