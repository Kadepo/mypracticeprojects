import React from "react";
import {IoLogOutOutline} from "react-icons/io5";

export const SignIcon = () => {
    return (
        <IoLogOutOutline size="1.75em"/>
    )
}