import React from "react";
import {IoMenuOutline} from "react-icons/io5";

export const MenuIcon = () => {
    return (
        <IoMenuOutline/>
    )
}