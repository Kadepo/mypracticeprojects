import React from "react";
import {IoHelpOutline} from "react-icons/io5";

export const HelpIcon = () => {
    return (
        <IoHelpOutline size="1.75em"/>
    )
}