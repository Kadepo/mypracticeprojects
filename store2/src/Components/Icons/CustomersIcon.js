import React from "react";
import {IoPeopleOutline} from "react-icons/io5";

export const CustomersIcon = () => {
    return (
        <IoPeopleOutline size="1.75em"/>
    )
}