import React from "react";
import {IoEyeOutline} from "react-icons/io5";

export const EyeIcon = () => {
    return (
        <IoEyeOutline />
    )
}