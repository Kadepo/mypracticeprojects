import React from "react";
import {IoCartOutline} from "react-icons/io5";

export const CartIcon = () => {
    return (
        <IoCartOutline />
    )
}