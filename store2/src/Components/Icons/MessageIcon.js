import React from "react";
import {IoChatbubbleOutline} from "react-icons/io5";

export const MessageIcon = () => {
    return (
        <IoChatbubbleOutline size="1.75em"/>
    )
}