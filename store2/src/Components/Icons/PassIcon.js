import React from "react";
import {IoLockClosedOutline} from "react-icons/io5";

export const PassIcon = () => {
    return (
        <IoLockClosedOutline size="1.75em"/>
    )
}