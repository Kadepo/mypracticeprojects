import React from "react";
import {IoChatbubblesOutline} from "react-icons/io5";

export const ChatIcon = () => {
    return (
        <IoChatbubblesOutline />
    )
}