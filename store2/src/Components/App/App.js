import React from "react";
import styles from "./App.module.css"
import {Navigation} from "../Navigation/Navigation";
import {routServices} from "../../services/routServices";
import {Header} from "../Header/Header";

export const App = () => {
    const router = routServices()

    return (
        <div className={styles.container}>
            <Navigation/>
            <Header/>
            {router}
        </div>
    )
}