import React,{useEffect, useState} from "react";
import styles from "./Navigation.module.css"
import {OneIcon} from "../Icons/OneIcon";
import {HomeIcon} from "../Icons/HomeIcon";
import {CustomersIcon} from "../Icons/CustomersIcon";
import {MessageIcon} from "../Icons/MessageIcon";
import {HelpIcon} from "../Icons/HelpIcon";
import {SettingsIcon} from "../Icons/SettingsIcon";
import {PassIcon} from "../Icons/PassIcon";
import {SignIcon} from "../Icons/SignIcon";
import {NavLink} from "react-router-dom";
import NavMotion from "../../services/NavMotion";
import {observer} from "mobx-react-lite";

export const Navigation = observer(() => {
    const [fullWidth, useWidth] = useState('300px')

    useEffect(()=> {
        if (NavMotion.navState === true) {
            useWidth('300px')
        }
        if (NavMotion.navState === false) {
            useWidth('80px')
        }
        console.log(NavMotion.navState)
    },[NavMotion.navState])

    const slideNav = {
        width : fullWidth
    }

    return (
        <div className={styles.navigation} style={slideNav}>
            <ul>
                <li>
                    <NavLink to={'/main'}>
                        <span className={styles.icon}><OneIcon/></span>
                        <span className={styles.title}>Brand Name</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/main'}>
                        <span className={styles.icon}><HomeIcon/></span>
                        <span className={styles.title}>Dashboard</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/customers'}>
                        <span className={styles.icon}><CustomersIcon/></span>
                        <span className={styles.title}>Customers</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/message'}>
                        <span className={styles.icon}><MessageIcon/></span>
                        <span className={styles.title}>Message</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/help'}>
                        <span className={styles.icon}><HelpIcon/></span>
                        <span className={styles.title}>Help</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/settings'}>
                        <span className={styles.icon}><SettingsIcon/></span>
                        <span className={styles.title}>Settings</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/auth'}>
                        <span className={styles.icon}><PassIcon/></span>
                        <span className={styles.title}>Password</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/auth'}>
                        <span className={styles.icon}><SignIcon/></span>
                        <span className={styles.title}>Sign Out</span>
                    </NavLink>
                </li>
            </ul>
        </div>
    )
})