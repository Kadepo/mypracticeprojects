import React from "react";
import styles from "./Cards.module.css"
import {EyeIcon} from "../../Icons/EyeIcon";
import {ChatIcon} from "../../Icons/ChatIcon";
import {CashIcon} from "../../Icons/CashIcon";
import {CartIcon} from "../../Icons/CartIcon";

export const Cards = () => {
    return(
        <div className={styles.cardBox}>
            <div className={styles.card}>
                <div>
                    <div className={styles.numbers}>1,504</div>
                    <div className={styles.cardName}>Daily Views</div>
                </div>
                <div className={styles.iconBx}>
                    <EyeIcon/>
                </div>
            </div>
            <div className={styles.card}>
                <div>
                    <div className={styles.numbers}>80</div>
                    <div className={styles.cardName}>Sales</div>
                </div>
                <div className={styles.iconBx}>
                    <CartIcon/>
                </div>
            </div>
            <div className={styles.card}>
                <div>
                    <div className={styles.numbers}>284</div>
                    <div className={styles.cardName}>Comments</div>
                </div>
                <div className={styles.iconBx}>
                    <ChatIcon/>
                </div>
            </div>
            <div className={styles.card}>
                <div>
                    <div className={styles.numbers}>$7,842</div>
                    <div className={styles.cardName}>Earning</div>
                </div>
                <div className={styles.iconBx}>
                    <CashIcon/>
                </div>
            </div>
        </div>
    )
}
