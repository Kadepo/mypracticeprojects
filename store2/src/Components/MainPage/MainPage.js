import React, {useEffect, useState} from "react";
import {Header} from "../Header/Header"
import {Cards} from "./Cards/Cards";
import styles from  "./MainPage.module.css"
import {DataList} from "./DataList/DataList";
import {observer} from "mobx-react-lite";
import NavMotion from "../../services/NavMotion";


export const MainPage = observer(() => {
    const [fullWidth, useWidth] = useState('calc(100% - 300px)')

    useEffect(()=> {
        if (NavMotion.navState === true) {
            useWidth('calc(100% - 300px)')
        }
        if (NavMotion.navState === false) {
            useWidth('calc(100% - 80px)')
        }
        console.log(NavMotion.navState)
    },[NavMotion.navState])

    const [indentLeft, useLeft] = useState('300px')

    useEffect(()=> {
        if (NavMotion.navState === true) {
            useLeft('300px')
        }
        if (NavMotion.navState === false) {
            useLeft('80px')
        }
        console.log(NavMotion.navState)
    },[NavMotion.navState])

    const slideNav = {
        left : indentLeft,
        width : fullWidth
    }

    return (
        <div style={slideNav} className={styles.main}>
            <Cards/>
            <DataList/>
        </div>
    )
})
