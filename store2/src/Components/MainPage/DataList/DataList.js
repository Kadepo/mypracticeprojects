import React from "react";
import styles from "./DataList.module.css"
import {OrderDetails} from "./OrderDetails/OrderDetails";
import {NewCustomers} from "./NewCustomers/NewCustomers";

export const DataList = () => {
    return(
        <div className={styles.details}>
            <OrderDetails/>
            <NewCustomers/>
        </div>
    )
}