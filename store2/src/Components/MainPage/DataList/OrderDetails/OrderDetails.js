import React from "react";
import styles from "./OrderDetails.module.css";

export const OrderDetails = () => {
    return(
        <div className={styles.recentOrders}>
            <div className={styles.cardHeader}>
                <h2>Recent Orders</h2>
                <a href="#" className={styles.btn}>View All</a>
            </div>
            <table>
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Price</td>
                    <td>Payment</td>
                    <td>Status</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Star Refrigerator</td>
                    <td>$1200</td>
                    <td>Paid</td>
                    <td><span className={styles.statusDelivered}>Delivered</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Window Coolers</td>
                    <td>$110</td>
                    <td>Due</td>
                    <td><span className={styles.statusPending}>Pending</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Speakers</td>
                    <td>$620</td>
                    <td>Paid</td>
                    <td><span className={styles.statusReturn}>Return</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Hp Laptop</td>
                    <td>$110</td>
                    <td>Due</td>
                    <td><span className={styles.statusProgress}>In Progress</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Apple Watch</td>
                    <td>$1200</td>
                    <td>Paid</td>
                    <td><span className={styles.statusDelivered}>Delivered</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Wall Fan</td>
                    <td>$110</td>
                    <td>Paid</td>
                    <td><span className={styles.statusPending}>Pending</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Adidas Shoes</td>
                    <td>$630</td>
                    <td>Paid</td>
                    <td><span className={styles.statusReturn}>Return</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Denim Shirts</td>
                    <td>$110</td>
                    <td>Due</td>
                    <td><span className={styles.statusProgress}>In Progress</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Casual Shoes</td>
                    <td>$575</td>
                    <td>Paid</td>
                    <td><span className={styles.statusPending}>Pending</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Wall Fan</td>
                    <td>$1200</td>
                    <td>Paid</td>
                    <td><span className={styles.statusPending}>Pending</span></td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>Denim Shirts</td>
                    <td>$1200</td>
                    <td>Due</td>
                    <td><span className={styles.statusProgress}>In Progress</span></td>
                </tr>
                </tbody>
            </table>
        </div>
    )
}