import React from "react";
import styles from "./NewCustomers.module.css"

export const NewCustomers = () => {
    return(
        <div className={styles.recentCustomers}>
            <div className={styles.cardHeader}>
                <h2>Recent Customers</h2>
            </div>
            <table>
                <tr>
                    <td width="60px">
                        <div className={styles.imgBx}>
                            <img src="https://sun9-48.userapi.com/impg/_WhdG6jO3wuHre6GbDL1TVGS10rRVyikHVPBbw/g91R3uKxbtM.jpg?size=300x300&quality=96&sign=57db7c6519f85516ab9c1a93dcc38c01&type=album" />
                        </div>
                    </td>
                    <td><h4>David<br/><span>Italy</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div className={styles.imgBx}>
                            <img src="https://sun9-43.userapi.com/impg/0fs9PHtA-n48IAXHENv67EzGmxwbyWOuEymAgA/jS26roa6ElA.jpg?size=300x300&quality=96&sign=b0580a9420e54497054b23a47cc1ee41&type=album" />
                        </div>
                    </td>
                    <td><h4>Muhammad<br/><span>India</span></h4></td>
                </tr>
                <tr>
                    <td width="60px">
                        <div className={styles.imgBx}>
                            <img src="https://sun9-87.userapi.com/impg/SkNb-Xam8kfSyH4PqyJOlb2cU22ykF0NZsH6uw/jgdHBMzcNQc.jpg?size=300x300&quality=96&sign=70a2efed27f85ec786980fa702856205&type=album" />
                        </div>
                    </td>
                    <td><h4>Amelia<br/><span>France</span></h4></td>
                </tr><tr>
                <td width="60px">
                    <div className={styles.imgBx}>
                        <img src="https://sun9-2.userapi.com/impg/8XNakXsZQZkdx3Mv409h5EC7sLilkcWNk1mk7Q/oNgfSZqOLak.jpg?size=300x300&quality=96&sign=a7aaaddd47aaf1c23d7db0ea2f4e8350&type=album" />
                    </div>
                </td>
                <td><h4>Olivia<br/><span>USA</span></h4></td>
            </tr><tr>
                <td width="60px">
                    <div className={styles.imgBx}>
                        <img src="https://sun9-9.userapi.com/impg/S7dkLRFMF57RAD2Aj0l89Y3Oxq36DpKK4NBHSg/HeBba93ycJ8.jpg?size=300x300&quality=96&sign=13b8f99aa202539c38f5201b44957e8c&type=album" />
                    </div>
                </td>
                <td><h4>Amit<br/><span>Japan</span></h4></td>
            </tr><tr>
                <td width="60px">
                    <div className={styles.imgBx}>
                        <img src="https://sun9-67.userapi.com/impg/GXsxl2w6QrJzL22W0wA3j89CRnYnjW4-QFJ_aQ/DpMJ72BEOgk.jpg?size=300x300&quality=96&sign=6471632e9436e60b11c2e205f962c842&type=album" />
                    </div>
                </td>
                <td><h4>Ashraf<br/><span>India</span></h4></td>
            </tr><tr>
                <td width="60px">
                    <div className={styles.imgBx}>
                        <img src="https://sun9-88.userapi.com/impg/y47SQGnn5AeslA4rJwKFYM92GhtmJjdu7kqryQ/bQnpabolXBg.jpg?size=300x300&quality=96&sign=f683b6f58e7c2d8b0e43f8077c9ecc66&type=album" />
                    </div>
                </td>
                <td><h4>Diana<br/><span>Malaysia</span></h4></td>
            </tr><tr>
                <td width="60px">
                    <div className={styles.imgBx}>
                        <img src="https://sun9-23.userapi.com/impg/m5ljQ5lRSfeOitFv-MlVLBkJIWML1BqYdTaNCA/DouZuTEJIpM.jpg?size=300x300&quality=96&sign=9a5c356df2280f1df5c81f09a799e2b0&type=album" />
                    </div>
                </td>
                <td><h4>Amit<br/><span>India</span></h4></td>
            </tr>
            </table>
        </div>
    )
}