import {Routes, Route} from 'react-router-dom'
import {MainPage} from "../pages/MainPage/MainPage";
import NewsPage from "../pages/NewsPage/NewsPage";

export const RoutesServices = () => {
    return(
        <Routes>
            <Route path={'/news/:id'} element={<NewsPage/>}/>
            <Route path={'/news'} element={<NewsPage/>}/>
            <Route path={'/'} element={<MainPage/>}/>
        </Routes>
    )
}