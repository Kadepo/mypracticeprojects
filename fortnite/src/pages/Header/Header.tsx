import React, {FC} from "react";
import style from './Header.module.scss'
import {AiOutlineSearch} from "react-icons/ai"
import {MdLanguage} from "react-icons/md"
import {RiUserHeartLine} from "react-icons/ri"
import {NavLink} from "react-router-dom";

const Header: FC = () => {
    return(
        <div className={style.wrapper}>
            <div className={style.leftNav}>
            <div className={style.icon}>
            </div>
                <a href="/" className={style.fortIcon}></a>
            <div className={style.wrapper1}>
                <ul className={style.nav}>
                    <li><NavLink to="/"><p>режимы</p></NavLink></li>
                    <li><NavLink to="/"><p>боевой пропуск</p></NavLink></li>
                    <li><NavLink to="/"><p>команда</p></NavLink></li>
                    <li><NavLink to="/"><p>в-баксы</p></NavLink></li>
                    <li><NavLink to="/"><p>соревнования</p></NavLink></li>
                    <li><NavLink to="/news"><p>новости</p></NavLink></li>
                    <li><NavLink to="/"><p>косплей</p></NavLink></li>
                    <li><NavLink to="/"><p>помощь</p></NavLink></li>
                </ul>
            </div>
            </div>
            <div className={style.rightNav}>
                <AiOutlineSearch/>
                <MdLanguage/>
                <a>
                    <RiUserHeartLine/>
                    <p>вход</p>
                </a>
                <button>загрузить</button>
            </div>
        </div>
    )
}

export default Header