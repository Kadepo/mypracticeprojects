import React,{FC} from "react";
import style from './NewsCard.module.scss'
import {NavLink} from "react-router-dom";

type Props = {
    image:string
    date:string
    title:string
    id:string
}

const NewsCard: FC<Props> = ({image, date, title, id})  => {

    return(
        <NavLink to={'/news/:id'}>
            <div className={style.wrapper} style={{backgroundImage: `url(${image})`}}>
                <h3>{date || title}</h3>
            </div>
        </NavLink>
    )
}

export default NewsCard