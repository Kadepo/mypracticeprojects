import React, {FC, useEffect, useState} from "react";
import style from "./NewsPage.module.scss"
import axios from "axios";
import {apiConfig} from "../../components/api_config/api_config";
import NewsCard from "./NewsCard/NewsCard";

const NewsPage: FC = () => {
    const [items, setItems] = useState([])
    useEffect(() => {
        axios(apiConfig.API_NEWS, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: apiConfig.API_KEY
            }
        })
            .then((response) => {
                setItems(response.data.news)
            })

    }, [])


    return(
        <div className={style.wrapper}>
            {items.map(i => (<NewsCard id={i.id} image={i.image} date={i.date} title={i.title}/>))}
        </div>
    )
}
export default NewsPage