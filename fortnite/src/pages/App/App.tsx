import React, {FC} from "react";
import Header from "../Header/Header";
import {RoutesServices} from "../../services/routesServices";
import {BrowserRouter} from "react-router-dom";



export const App: FC = () => {
    const router = RoutesServices()

    return(
        <BrowserRouter>
            <Header/>
            {router}
        </BrowserRouter>
    )
}